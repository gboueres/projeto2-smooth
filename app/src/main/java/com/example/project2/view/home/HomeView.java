package com.example.project2.view.home;

import com.example.project2.view.BaseView;


public interface HomeView extends BaseView {

    void welcome(String msg);
}
