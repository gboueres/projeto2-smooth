package com.example.project2.view;

public interface BaseView {

    void showDialogError(String message);
}
