package com.example.project2.model.gets;

import com.example.project2.model.Item;

import java.util.List;

import io.realm.RealmObject;



public class Feed {

    private List<DataFeed> data;

    public List<DataFeed> getData() {

        return data;
    }

    public void setData(List<DataFeed> data) {

        this.data = data;
    }
}
