package com.example.project2.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.project2.R;
import com.example.project2.model.post.DataOffers;

import java.util.List;



public class OffersFeedAdapter extends RecyclerView.Adapter<OffersFeedAdapter.CustomViewHolder> {


    private List<DataOffers> dataList;

    private Context context;

    public OffersFeedAdapter(Context context, List<DataOffers> dataList) {

        this.context = context;
        this.dataList = dataList;
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public ImageView imageViewBackground;

        public ImageView imageView;

        CustomViewHolder(View itemView) {

            super(itemView);
            mView = itemView;

            imageViewBackground = mView.findViewById(R.id.imageViewOffersBackground);
//            imageView = mView.findViewById(R.id.imageView);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context.getApplicationContext(), "HORIZONTAL VIEW CLICKED", Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflamos nosso layout
//        switch ()
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.inflate_card_offer, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

//        switch ()
        Glide.with(context).
                load(dataList.get(position).getImage_url())
                .centerCrop()
                .into(holder.imageViewBackground);
        //configuramos nossos dados dentro de nosso xml
//        holder.txtTitle.setText(dataList.get(position).getTitle());
//        Glide.with(context).load(dataList.get(position).getThumbnailUrl()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        //retorna o tamanho de nossa lista
        return dataList.size();
    }

//    public void alteraAlgo()
//    {
//        dataList.remove(0);
//        notifyDataSetChanged();
//    }


    @Override
    public int getItemViewType(int position) {

        switch (dataList.get(position).getOffer_type()) {
            default:
                return super.getItemViewType(position);
        }
    }

}

