package com.example.project2.view.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.project2.Adapter.DataFeedAdapter;
import com.example.project2.R;
import com.example.project2.model.SingletonSession;
import com.example.project2.model.gets.DataBusiness;
import com.example.project2.model.gets.DataFeed;
import com.example.project2.model.gets.Feed;
import com.example.project2.presenter.home.HomePresenter;
import com.example.project2.service.SmoothService;
import com.example.project2.view.MapsActivity;
import com.example.project2.view.login.LoginActivity;

import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class HomeActivity extends AppCompatActivity implements HomeView, BottomNavigationView.OnNavigationItemSelectedListener {
    HomePresenter homePresenter;
    private RecyclerView recyclerView;
    private DataFeedAdapter dataFeedAdapter;
    //Button buttonlogout;
    Toolbar main_toolbar;

    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);


        recyclerView = findViewById(R.id.recyclerView);
        //buttonlogout = findViewById(R.id.button_logout);

        main_toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        main_toolbar.setLogo(R.drawable.sc);
        setSupportActionBar(main_toolbar);





        String idSession = getIntent().getExtras().getString("id_session");

        System.out.print("ID SESSION " + idSession);
        int rememberuser = getIntent().getExtras().getInt("rememberuser");


        SingletonSession.Instance().setIdsession(idSession);

        //SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences preferences = getSharedPreferences("com.blabla.yourapp", Context.MODE_PRIVATE);
        if (rememberuser == 1) {


            preferences.edit().putString("session", idSession).commit();

        }


        //SharedPreferences.Editor editor = settings.edit();
        //editor.putString("id_session_disk", idSession);


//        buttonlogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                preferences.edit().putString("session", null).commit();
//                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
//                startActivity(intent);
//            }
//        });


        //TODO  NECESSARIO VERIFICAR ROTA DO BACKEND NO ARQUIVO "smoothApi"
        //-------------------------------------------------
        SmoothService smoothService = new SmoothService();
//
        Call<Feed> requestFeed = smoothService
                .getSmoothApi()
                .getFeed(idSession);
        //-------------------------------------------------

        //TODO localhost
        //-------------------------------------------------
//                LocalService localService = new LocalService();
//                final Call<Feed> requestFeed = localService.getLocalService().getFeed();
        //-------------------------------------------------

        requestFeed.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {

                Iterator it = response.body().getData().iterator();
                populateRecyclerView(response.body().getData());
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {

                System.err.println(t.getCause());
                System.err.println(t.getMessage());
            }
        });



        homePresenter = new HomePresenter(this);
        homePresenter.sucess();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch ( item.getItemId() ){
            case R.id.action_sair:
                //fazer algo
                deslogarUsuario();
                return true;
            case R.id.action_configuracoes:
                return true;
            case R.id.action_compartilhar:
                gotowebpage();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deslogarUsuario(){
        final SharedPreferences preferences = getSharedPreferences("com.blabla.yourapp", Context.MODE_PRIVATE);
        preferences.edit().putString("session", null).commit();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void gotowebpage(){

        String url = "http://smoothcommerce.tech/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

    private void populateRecyclerView(List<DataFeed> data) {

        dataFeedAdapter = new DataFeedAdapter(data, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(dataFeedAdapter);
    }

    @Override
    public void welcome(String msg) {

        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void showDialogError(String message) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.feed:
                break;
            case R.id.maps:
                startActivity(new Intent(HomeActivity.this, MapsActivity.class));
                System.out.println("maps");
                break;

        }
        return false;
    }
}
