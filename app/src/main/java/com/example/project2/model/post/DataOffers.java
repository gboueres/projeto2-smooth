package com.example.project2.model.post;

import java.util.List;



public class DataOffers {
    /**
     * offer_id : 205
     * offer_type : discounted-item
     * offer_details : {"discount":"1","points_cost":0,"discount_type":"dollars","apply_multiple":"true"}
     * label : $1 off a combo
     * description : Get $1 off a hand-crafted beverage when you purchase a box
     * title :
     * subtitle :
     * details :
     * reusable : true
     * auto_redeem : true
     * current_balance : 0
     * business_id : 0
     * business_name :
     * eligible : true
     * activated : true
     * image_url : https://s3.amazonaws.com/assets.smoothpay.com/store_images/basilbox/offers/MealDiscount.jpg
     * mobile_conditions : {"required_products":[2,3,4,5,17,18,174],"discounted_products":[1],"required_product_condition":"OR"}
     * web_conditions : null
     * required_products : [{"product_id":2,"name":"Mango Ginger Ale Soda","user_description":"Our ultra-refreshing Mango Ginger Ale Soda is carefully crafted with pure cane sugar. It�s GMO free, and best of all, it�s delicious.","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/MangoGingerAleSoda.jpg","tag":"123 Calories","is_featured":true,"is_product_available":true,"price":3.95,"variant_name":"Regular","variant_id":2,"has_valid_configuration":true,"tags":[],"prices":[{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]},{"product_id":4,"name":"Thai Iced Tea","user_description":"A beverage that is extremely popular on the streets of Thailand. Our version is made with natural cane sugar that is both GMO free and Fairtrade Certified.","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/ThaiIcedTea.jpg","tag":"310 Calories","is_featured":true,"is_product_available":true,"price":3.95,"variant_name":"Regular","variant_id":2,"has_valid_configuration":true,"tags":[],"prices":[{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]},{"product_id":3,"name":"Passion Fruit Lychee Soda","user_description":"Southeast Asian inspired passion fruit and lychee flavours are infused into this tropical soda, full of flavour and made with pure cane sugar. GMO free.","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/PassionFruitLycheeSoda.jpg","tag":"123 Calories","is_featured":true,"is_product_available":true,"price":3.95,"variant_name":"Regular","variant_id":2,"has_valid_configuration":true,"tags":[],"prices":[{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]},{"product_id":5,"name":"Pineapple Rose Soda","user_description":"Pineapple paired with rose flower - a tropical paradise. Crafted with pure cane sugar, this GMO free refresher will tantalize your taste buds.","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/PineappleRoseSoda.jpg","tag":"125 Calories","is_featured":true,"is_product_available":true,"price":3.95,"variant_name":"Regular","variant_id":2,"has_valid_configuration":true,"tags":[],"prices":[{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]},{"product_id":17,"name":"Lychee Rose Green Tea","user_description":"Premium organic loose leaf green tea with hints of lychee and rose.","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/LycheeRoseGreenTea.jpg","tag":"100 Calories","is_featured":true,"is_product_available":true,"price":3.95,"variant_name":"Regular","variant_id":2,"has_valid_configuration":true,"tags":[],"prices":[{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]},{"product_id":18,"name":"Mango Apple Cider","user_description":"All natural spiced apple cider with a mango twist.","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/MangoAppleCider.jpg","tag":"240 Calories","is_featured":true,"is_product_available":true,"price":3.95,"variant_name":"Regular","variant_id":2,"has_valid_configuration":true,"tags":[],"prices":[{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]}]
     * discounted_products : [{"product_id":1,"name":"Build Your Own Box","user_description":"","image_url":"http://assets.smoothpay.com/store_images/basilbox/menu/box.jpg","tag":"0 Calories","is_featured":true,"is_product_available":true,"price":11.45,"variant_name":"Regular","variant_id":1,"has_valid_configuration":false,"tags":[],"prices":[{"variant_id":1,"name":"Regular","price":11.45,"is_default":true}]}]
     * eligible_products : []
     */

    private int offer_id;
    private String offer_type;
    private OfferDetails offer_details;
    private String label;
    private String description;
    private String title;
    private String subtitle;
    private String details;
    private boolean reusable;
    private boolean auto_redeem;
    private String current_balance;
    private int business_id;
    private String business_name;
    private boolean eligible;
    private boolean activated;
    private String image_url;
    private MobileConditions mobile_conditions;
    private Object web_conditions;
    private List<RequiredProducts> required_products;
    private List<DiscountedProducts> discounted_products;
    private List<?> eligible_products;

    public int getOffer_id() {

        return offer_id;
    }

    public void setOffer_id(int offer_id) {

        this.offer_id = offer_id;
    }

    public String getOffer_type() {

        return offer_type;
    }

    public void setOffer_type(String offer_type) {

        this.offer_type = offer_type;
    }

    public OfferDetails getOffer_details() {

        return offer_details;
    }

    public void setOffer_details(OfferDetails offer_details) {

        this.offer_details = offer_details;
    }

    public String getLabel() {

        return label;
    }

    public void setLabel(String label) {

        this.label = label;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getSubtitle() {

        return subtitle;
    }

    public void setSubtitle(String subtitle) {

        this.subtitle = subtitle;
    }

    public String getDetails() {

        return details;
    }

    public void setDetails(String details) {

        this.details = details;
    }

    public boolean isReusable() {

        return reusable;
    }

    public void setReusable(boolean reusable) {

        this.reusable = reusable;
    }

    public boolean isAuto_redeem() {

        return auto_redeem;
    }

    public void setAuto_redeem(boolean auto_redeem) {

        this.auto_redeem = auto_redeem;
    }

    public String getCurrent_balance() {

        return current_balance;
    }

    public void setCurrent_balance(String current_balance) {

        this.current_balance = current_balance;
    }

    public int getBusiness_id() {

        return business_id;
    }

    public void setBusiness_id(int business_id) {

        this.business_id = business_id;
    }

    public String getBusiness_name() {

        return business_name;
    }

    public void setBusiness_name(String business_name) {

        this.business_name = business_name;
    }

    public boolean isEligible() {

        return eligible;
    }

    public void setEligible(boolean eligible) {

        this.eligible = eligible;
    }

    public boolean isActivated() {

        return activated;
    }

    public void setActivated(boolean activated) {

        this.activated = activated;
    }

    public String getImage_url() {

        return image_url;
    }

    public void setImage_url(String image_url) {

        this.image_url = image_url;
    }

    public MobileConditions getMobile_conditions() {

        return mobile_conditions;
    }

    public void setMobile_conditions(MobileConditions mobile_conditions) {

        this.mobile_conditions = mobile_conditions;
    }

    public Object getWeb_conditions() {

        return web_conditions;
    }

    public void setWeb_conditions(Object web_conditions) {

        this.web_conditions = web_conditions;
    }

    public List<RequiredProducts> getRequired_products() {

        return required_products;
    }

    public void setRequired_products(List<RequiredProducts> required_products) {

        this.required_products = required_products;
    }

    public List<DiscountedProducts> getDiscounted_products() {

        return discounted_products;
    }

    public void setDiscounted_products(List<DiscountedProducts> discounted_products) {

        this.discounted_products = discounted_products;
    }

    public List<?> getEligible_products() {

        return eligible_products;
    }

    public void setEligible_products(List<?> eligible_products) {

        this.eligible_products = eligible_products;
    }

    public static class OfferDetails {
        /**
         * discount : 1
         * points_cost : 0
         * discount_type : dollars
         * apply_multiple : true
         */

        private String discount;
        private int points_cost;
        private String discount_type;
        private String apply_multiple;

        public String getDiscount() {

            return discount;
        }

        public void setDiscount(String discount) {

            this.discount = discount;
        }

        public int getPoints_cost() {

            return points_cost;
        }

        public void setPoints_cost(int points_cost) {

            this.points_cost = points_cost;
        }

        public String getDiscount_type() {

            return discount_type;
        }

        public void setDiscount_type(String discount_type) {

            this.discount_type = discount_type;
        }

        public String getApply_multiple() {

            return apply_multiple;
        }

        public void setApply_multiple(String apply_multiple) {

            this.apply_multiple = apply_multiple;
        }
    }


    public static class MobileConditions {
        /**
         * required_products : [2,3,4,5,17,18,174]
         * discounted_products : [1]
         * required_product_condition : OR
         */

        private String required_product_condition;
        private List<Integer> required_products;
        private List<Integer> discounted_products;

        public String getRequired_product_condition() {

            return required_product_condition;
        }

        public void setRequired_product_condition(String required_product_condition) {

            this.required_product_condition = required_product_condition;
        }

        public List<Integer> getRequired_products() {

            return required_products;
        }

        public void setRequired_products(List<Integer> required_products) {

            this.required_products = required_products;
        }

        public List<Integer> getDiscounted_products() {

            return discounted_products;
        }

        public void setDiscounted_products(List<Integer> discounted_products) {

            this.discounted_products = discounted_products;
        }
    }


    public static class RequiredProducts {
        /**
         * product_id : 2
         * name : Mango Ginger Ale Soda
         * user_description : Our ultra-refreshing Mango Ginger Ale Soda is carefully crafted with pure cane sugar. It�s GMO free, and best of all, it�s delicious.
         * image_url : http://assets.smoothpay.com/store_images/basilbox/menu/MangoGingerAleSoda.jpg
         * tag : 123 Calories
         * is_featured : true
         * is_product_available : true
         * price : 3.95
         * variant_name : Regular
         * variant_id : 2
         * has_valid_configuration : true
         * tags : []
         * prices : [{"variant_id":2,"name":"Regular","price":3.95,"is_default":true}]
         */

        private int product_id;
        private String name;
        private String user_description;
        private String image_url;
        private String tag;
        private boolean is_featured;
        private boolean is_product_available;
        private double price;
        private String variant_name;
        private int variant_id;
        private boolean has_valid_configuration;
        private List<?> tags;
        private List<Prices> prices;

        public int getProduct_id() {

            return product_id;
        }

        public void setProduct_id(int product_id) {

            this.product_id = product_id;
        }

        public String getName() {

            return name;
        }

        public void setName(String name) {

            this.name = name;
        }

        public String getUser_description() {

            return user_description;
        }

        public void setUser_description(String user_description) {

            this.user_description = user_description;
        }

        public String getImage_url() {

            return image_url;
        }

        public void setImage_url(String image_url) {

            this.image_url = image_url;
        }

        public String getTag() {

            return tag;
        }

        public void setTag(String tag) {

            this.tag = tag;
        }

        public boolean isIs_featured() {

            return is_featured;
        }

        public void setIs_featured(boolean is_featured) {

            this.is_featured = is_featured;
        }

        public boolean isIs_product_available() {

            return is_product_available;
        }

        public void setIs_product_available(boolean is_product_available) {

            this.is_product_available = is_product_available;
        }

        public double getPrice() {

            return price;
        }

        public void setPrice(double price) {

            this.price = price;
        }

        public String getVariant_name() {

            return variant_name;
        }

        public void setVariant_name(String variant_name) {

            this.variant_name = variant_name;
        }

        public int getVariant_id() {

            return variant_id;
        }

        public void setVariant_id(int variant_id) {

            this.variant_id = variant_id;
        }

        public boolean isHas_valid_configuration() {

            return has_valid_configuration;
        }

        public void setHas_valid_configuration(boolean has_valid_configuration) {

            this.has_valid_configuration = has_valid_configuration;
        }

        public List<?> getTags() {

            return tags;
        }

        public void setTags(List<?> tags) {

            this.tags = tags;
        }

        public List<Prices> getPrices() {

            return prices;
        }

        public void setPrices(List<Prices> prices) {

            this.prices = prices;
        }

        public static class Prices {
            /**
             * variant_id : 2
             * name : Regular
             * price : 3.95
             * is_default : true
             */

            private int variant_id;
            private String name;
            private double price;
            private boolean is_default;

            public int getVariant_id() {

                return variant_id;
            }

            public void setVariant_id(int variant_id) {

                this.variant_id = variant_id;
            }

            public String getName() {

                return name;
            }

            public void setName(String name) {

                this.name = name;
            }

            public double getPrice() {

                return price;
            }

            public void setPrice(double price) {

                this.price = price;
            }

            public boolean isIs_default() {

                return is_default;
            }

            public void setIs_default(boolean is_default) {

                this.is_default = is_default;
            }
        }
    }


    public static class DiscountedProducts {
        /**
         * product_id : 1
         * name : Build Your Own Box
         * user_description :
         * image_url : http://assets.smoothpay.com/store_images/basilbox/menu/box.jpg
         * tag : 0 Calories
         * is_featured : true
         * is_product_available : true
         * price : 11.45
         * variant_name : Regular
         * variant_id : 1
         * has_valid_configuration : false
         * tags : []
         * prices : [{"variant_id":1,"name":"Regular","price":11.45,"is_default":true}]
         */

        private int product_id;
        private String name;
        private String user_description;
        private String image_url;
        private String tag;
        private boolean is_featured;
        private boolean is_product_available;
        private double price;
        private String variant_name;
        private int variant_id;
        private boolean has_valid_configuration;
        private List<?> tags;
        private List<PricesX> prices;

        public int getProduct_id() {

            return product_id;
        }

        public void setProduct_id(int product_id) {

            this.product_id = product_id;
        }

        public String getName() {

            return name;
        }

        public void setName(String name) {

            this.name = name;
        }

        public String getUser_description() {

            return user_description;
        }

        public void setUser_description(String user_description) {

            this.user_description = user_description;
        }

        public String getImage_url() {

            return image_url;
        }

        public void setImage_url(String image_url) {

            this.image_url = image_url;
        }

        public String getTag() {

            return tag;
        }

        public void setTag(String tag) {

            this.tag = tag;
        }

        public boolean isIs_featured() {

            return is_featured;
        }

        public void setIs_featured(boolean is_featured) {

            this.is_featured = is_featured;
        }

        public boolean isIs_product_available() {

            return is_product_available;
        }

        public void setIs_product_available(boolean is_product_available) {

            this.is_product_available = is_product_available;
        }

        public double getPrice() {

            return price;
        }

        public void setPrice(double price) {

            this.price = price;
        }

        public String getVariant_name() {

            return variant_name;
        }

        public void setVariant_name(String variant_name) {

            this.variant_name = variant_name;
        }

        public int getVariant_id() {

            return variant_id;
        }

        public void setVariant_id(int variant_id) {

            this.variant_id = variant_id;
        }

        public boolean isHas_valid_configuration() {

            return has_valid_configuration;
        }

        public void setHas_valid_configuration(boolean has_valid_configuration) {

            this.has_valid_configuration = has_valid_configuration;
        }

        public List<?> getTags() {

            return tags;
        }

        public void setTags(List<?> tags) {

            this.tags = tags;
        }

        public List<PricesX> getPrices() {

            return prices;
        }

        public void setPrices(List<PricesX> prices) {

            this.prices = prices;
        }

        public static class PricesX {
            /**
             * variant_id : 1
             * name : Regular
             * price : 11.45
             * is_default : true
             */

            private int variant_id;
            private String name;
            private double price;
            private boolean is_default;

            public int getVariant_id() {

                return variant_id;
            }

            public void setVariant_id(int variant_id) {

                this.variant_id = variant_id;
            }

            public String getName() {

                return name;
            }

            public void setName(String name) {

                this.name = name;
            }

            public double getPrice() {

                return price;
            }

            public void setPrice(double price) {

                this.price = price;
            }

            public boolean isIs_default() {

                return is_default;
            }

            public void setIs_default(boolean is_default) {

                this.is_default = is_default;
            }
        }
    }
}

