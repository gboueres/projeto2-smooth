package com.example.project2.view;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.example.project2.R;
import com.example.project2.model.SingletonSession;
import com.example.project2.model.gets.DataBusiness;
import com.example.project2.service.SmoothService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;

import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MapsActivity extends FragmentActivity implements  OnMapReadyCallback{

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        googleMap.setMinZoomPreference(5);
        SmoothService smoothBusiness = new SmoothService();
        Call<DataBusiness> requestBusiness = smoothBusiness
                .getSmoothApi()
                .getBusiness(SingletonSession.Instance().getIdsession());
        requestBusiness.enqueue(new Callback<DataBusiness>() {
            @Override
            public void onResponse(Call<DataBusiness> call, Response<DataBusiness> response) {
                System.out.println(response.body().getData());
                Iterator it = response.body().getData().iterator();
                while (it.hasNext()) {
                    DataBusiness.Data aux = (DataBusiness.Data) it.next();
                    LatLng maker = new LatLng(aux.getLatitude(), aux.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(maker).title(aux.getName()));

                   // mMap.addMarker(new MarkerOptions().position(maker).snippet("This is my spot!"));
                    //mMap.addMarker(new MarkerOptions().position(maker).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(maker));

                }
            }
            @Override
            public void onFailure(Call<DataBusiness> call, Throwable t) {
                System.err.println(t.getCause());
                System.err.println(t.getMessage());

            }

        });

    }



}
