package com.example.project2.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.project2.R;
import com.example.project2.model.SingletonSession;
import com.example.project2.model.gets.DataFeed;
import com.example.project2.model.post.DataOffers;
import com.example.project2.model.post.Offers;
import com.example.project2.service.SmoothService;
import com.example.project2.view.MapsActivity;
import com.example.project2.view.home.HomeActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class DataFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DataFeed> dataList;
    private Context context;
    static int positionfeed;


    public DataFeedAdapter(List<DataFeed> dataList, Context context) {

        this.dataList = dataList;
        this.context = context;
    }


    class CardViewHolder extends RecyclerView.ViewHolder {


        public View mView;
        public TextView textViewTitle;
        public TextView textViewSubTitle;
        public TextView textViewDescription;
        public ImageView imageViewBackground;

        public CardViewHolder(@NonNull final View itemView) {

            super(itemView);
            mView = itemView;
            final Integer position = positionfeed;

            textViewTitle = mView.findViewById(R.id.textViewTitle);
            textViewSubTitle = mView.findViewById(R.id.textViewSubTitle);
            textViewDescription = mView.findViewById(R.id.textViewDescription);
            imageViewBackground = mView.findViewById(R.id.imageViewBackground);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context.getApplicationContext(), "CARDVIEW CLICKED " + dataList.get(position).getItems().get(0).getCard_action()  , Toast.LENGTH_LONG).show();

                }
            });
        }
    }


    class HorizontalCardsViewHolder extends RecyclerView.ViewHolder {

        private RecyclerView recyclerView;
        private OffersFeedAdapter offersFeedAdapter;
        public View mView;

        HorizontalCardsViewHolder(View itemView) {

            super(itemView);
            mView = itemView;

            recyclerView = mView.findViewById(R.id.RecyclerViewHorizontalCards);




        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case 0:
                return new HorizontalCardsViewHolder(layoutInflater.inflate(R.layout.inflate_horinzontal, parent, false));
            case 1:
                return new CardViewHolder(layoutInflater.inflate(R.layout.inflate_single_card, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        positionfeed = position + 1;
        switch (holder.getItemViewType()) {
            case 0:
                final HorizontalCardsViewHolder horizontalCardsViewHolder = (HorizontalCardsViewHolder) holder;


                //TODO  NECESSARIO VERIFICAR ROTA DO BACKEND NO ARQUIVO "smoothApi"
                //-------------------------------------------------
                SmoothService smoothService = new SmoothService();
                String id = SingletonSession.Instance().getIdsession();

                final Call<Offers> requestFeed = smoothService.getSmoothApi().getOffers(id);
                //-------------------------------------------------

                //TODO localhost
                //-------------------------------------------------
//                LocalService localService = new LocalService();
//                final Call<Offers> requestFeed = localService.getLocalService().getOffers();
                //-------------------------------------------------
                requestFeed.enqueue(new Callback<Offers>() {
                    @Override
                    public void onResponse(Call<Offers> call, Response<Offers> response) {
                        populeHorizontalCardsViewHolder
                                (response.body().getData(), horizontalCardsViewHolder.recyclerView
                                );
                    }

                    @Override
                    public void onFailure(Call<Offers> call, Throwable t) {

                    }
                });

                break;

            case 1:
                CardViewHolder cardViewHolder = (CardViewHolder) holder;
                cardViewHolder.textViewTitle.setText(dataList.get(position).getItems().get(0).getTitle());
                cardViewHolder.textViewSubTitle.setText(dataList.get(position).getItems().get(0).getSubtitle());
                cardViewHolder.textViewDescription.setText(dataList.get(position).getItems().get(0).getDescription());
                Glide.
                        with(context).
                        load(dataList.get(position).getItems().get(0).getBackground_image_url())
                        .centerCrop()
                        .into(cardViewHolder.imageViewBackground);


                break;
        }
    }

    private void populeHorizontalCardsViewHolder(List<DataOffers> data, RecyclerView recyclerView) {

        System.out.println(data.size());
        OffersFeedAdapter offersFeedAdapter = new OffersFeedAdapter(context, data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(offersFeedAdapter);
    }


    @Override
    public int getItemCount() {

        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {

        switch (dataList.get(position).getFeed_style_type()) {
            case "horizontal_cards":
                return 0;
            case "single_card":
                return 1;
            case "single_compact_card":
                return 1;
        }
        return -1;
    }
}
