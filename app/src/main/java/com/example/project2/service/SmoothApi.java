package com.example.project2.service;

import com.example.project2.model.User;
import com.example.project2.model.UserAutoLogin;
import com.example.project2.model.gets.DataBusiness;
import com.example.project2.model.gets.Feed;
import com.example.project2.model.post.Offers;
import com.example.project2.model.post.PostAutoLogin;
import com.example.project2.model.post.PostLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SmoothApi {


    @POST("/v2_login")
    Call<User> login(@Body PostLogin postLogin);

    @POST("/v2_auto_login")
    Call<UserAutoLogin> autologin(@Body PostAutoLogin postAutoLogin);

    @GET("/consumer/v2/feed")
    Call<Feed> getFeed(@Header("X-SP-API-Token") String session_id);

    @GET("/consumer/v2/businesses")
    Call<DataBusiness> getBusiness(@Header("X-SP-API-Token") String session_id);

    //TODO VERIFICAR ROTAS DO BACK END

    @POST("/consumer/v2/offers")
    Call<Offers> getOffers(@Header("X-SP-API-Token") String session_id);



}
