/**/
package com.example.project2.presenter.login;

import android.widget.Toast;
/**/

/**/



/**/




















/**/

/**//**/
/**/







import com.example.project2.model.User;
import com.example.project2.model.UserAutoLogin;
import com.example.project2.model.post.PostAutoLogin;
import com.example.project2.model.post.PostLogin;
import com.example.project2.service.SmoothService;
import com.example.project2.view.login.LoginView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {

    LoginView loginView;
    SmoothService smoothService;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;

        if(this.smoothService == null)
        {
            this.smoothService = new SmoothService();
        }
    }

    public void login(PostLogin user) {

        Call<User> request = smoothService
                .getSmoothApi()
                .login(user);



        request.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                System.out.println(response.toString());
                System.out.println(response.message());

               if(response.isSuccessful())
               {
                   System.out.println("RESPONSE SUCCESSFUL");
                   if(response.body().getResult().equalsIgnoreCase("true"))
                   {
                       System.out.println("LOGIN OK");
                       loginView.confirmLogin(response.body());
                   }
                   else
                   {
                       loginView.showLoginIncorrect(response.body().getResult());
                       System.out.println("LOGIN NOK");
                   }
               }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                loginView.showDialogError(t.getMessage());
            }
        });

    }



    public void autologin(PostAutoLogin userautologin) {

        Call<UserAutoLogin> request = smoothService
                .getSmoothApi()
                .autologin(userautologin);



        request.enqueue(new Callback<UserAutoLogin>() {
            @Override
            public void onResponse(Call<UserAutoLogin> call, Response<UserAutoLogin> response) {

                System.out.println(response.toString());
                System.out.println(response.message());

                if(response.isSuccessful())
                {
                    System.out.println("RESPONSE AUTO LOGIN SUCCESSFUL");
                    if(response.body().getResult().equalsIgnoreCase("true"))
                    {
                        System.out.println("AUTO LOGIN OK");
                        loginView.confirmAutoLogin(response.body());
                    }
                    else
                    {
                        loginView.showLoginIncorrect(response.body().getResult());
                        System.out.println("AUTO LOGIN NOK");
                    }
                }

            }

            @Override
            public void onFailure(Call<UserAutoLogin> call, Throwable t) {
                loginView.showDialogError(t.getMessage());
            }
        });

    }
}
