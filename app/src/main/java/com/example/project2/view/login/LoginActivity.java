package com.example.project2.view.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.project2.R;
import com.example.project2.model.SingletonSession;
import com.example.project2.model.User;
import com.example.project2.model.UserAutoLogin;
import com.example.project2.model.post.PostAutoLogin;
import com.example.project2.model.post.PostLogin;
import com.example.project2.presenter.login.LoginPresenter;
import com.example.project2.view.home.HomeActivity;



public class LoginActivity extends AppCompatActivity implements LoginView {


    Button buttonLogin;
    EditText textLogin;
    EditText textPassword;
    RadioButton basilbox;
    RadioButton toppers;
    RadioButton oliva;
    RadioButton balzacs;
    RadioButton smokes;
    RadioButton chopped;
    RadioButton teriyaki;


    CheckBox rememberuser;
    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenter = new LoginPresenter(this);
        final PostLogin user = new PostLogin();
        final PostAutoLogin userAuto = new PostAutoLogin();

        textLogin = (EditText) findViewById(R.id.editText_E_mail);
        textPassword = (EditText) findViewById(R.id.editText_Password);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        basilbox = (RadioButton) findViewById(R.id.basilbox);
        toppers = (RadioButton) findViewById(R.id.toppers);
        oliva = (RadioButton) findViewById(R.id.oliva);
        balzacs = (RadioButton) findViewById(R.id.balzacs);
        smokes = (RadioButton) findViewById(R.id.smokes);
        chopped = (RadioButton) findViewById(R.id.chopped);
        teriyaki = (RadioButton) findViewById(R.id.teriyaki);



        rememberuser = (CheckBox) findViewById(R.id.rememberUser);

//      loginPresenter.login(user);




        //String id = SingletonSession.Instance().getIdsession();
        SharedPreferences preferences =
                getSharedPreferences("com.blabla.yourapp", Context.MODE_PRIVATE);

        String id = preferences.getString("session", null);
        userAuto.setDevice_token(user.getDevice_token());
        userAuto.setLatitude("40.98348973");
        userAuto.setLongitude("87.987324987");
        userAuto.setLocale("en");
        userAuto.setPlatform_id(2);
        userAuto.setSource("basilbox");
        userAuto.setSession_id(id);


        if (userAuto.getSession_id() != null)
        {
        loginPresenter.autologin(userAuto);
        }
        else
        {
            Toast.makeText(getApplicationContext(),id, Toast.LENGTH_LONG).show();
        }


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user.setEmail(textLogin.getText().toString());
                user.setPassword(textPassword.getText().toString());
                user.setLatitude("40.98348973");
                user.setLongitude("87.987324987");

                if (basilbox.isChecked()){
                    user.setSource("basilbox");
                }
                else if (toppers.isChecked()){
                    user.setSource("toppers");
                }
                else if (oliva.isChecked()){
                    user.setSource("oliva");
                }
                else if (balzacs.isChecked()){
                    user.setSource("balzacs");
                }
                else if (smokes.isChecked()){
                    user.setSource("smokes");
                }
                else if (chopped.isChecked()){
                    user.setSource("chopped");
                }
                else if (teriyaki.isChecked()){
                    user.setSource("teriyaki");
                }


                user.setLocale("en");
                user.setDevice_token("FHYCH4853fHcne3");
                user.setPlatform_id(2);
//            user.setEmail("sanjay@smooth.tech");
//            user.setPassword("1234");

                if (isValidEmail(textLogin.getText().toString()))
                    {

                        loginPresenter.login(user);

                    }
                else
                    {
                        Toast.makeText(getApplicationContext(), "E-mail not valid. Please, Re-enter", Toast.LENGTH_LONG).show();
                    }

                //Toast.makeText(getApplicationContext(), "Login Clicked", Toast.LENGTH_LONG).show();

                //TODO JUMP AUTH
//              startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//              finish();

            }
        });


    }

    @Override
    public void confirmLogin(final User user) {


        //Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();


        bundle.putString("id_session", user.getSession_id());


        if (rememberuser.isChecked()){
            bundle.putInt("rememberuser", 1);
        }
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        this.finish();


    }

    @Override
    public void confirmAutoLogin(final UserAutoLogin userautologin) {


        //Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putString("id_session", userautologin.getSession_id());

        System.out.println("CONFIRM AUTO LOGIN OK");
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        this.finish();


    }

    @Override
    public void showLoginIncorrect(String result) {

        Toast.makeText(this, "Login NOK. E-mail or Password incorrect. Try Again.", Toast.LENGTH_LONG).show();
    }


    @Override
    public void showDialogError(String message) {

        System.out.println("Error:" + message);
        Toast.makeText(this, "Error:" + message, Toast.LENGTH_LONG).show();
    }


    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
