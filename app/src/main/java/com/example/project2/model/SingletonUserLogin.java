package com.example.project2.model;
public class SingletonUserLogin {

    private static SingletonUserLogin instance;
    private UserAutoLogin userAutoLogin;
    //no outer class can initialize this class's object
    private SingletonUserLogin() {}

    public static SingletonUserLogin Instance()
    {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null)
        {
            instance = new SingletonUserLogin();
        }
        return instance;
    }

    public UserAutoLogin getUserAutoLogin() {
        return userAutoLogin;
    }

    public void setUserAutoLogin(UserAutoLogin userAutoLogin) {
        this.userAutoLogin = userAutoLogin;
    }
}
