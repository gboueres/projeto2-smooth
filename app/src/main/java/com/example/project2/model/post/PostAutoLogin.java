package com.example.project2.model.post;

public class PostAutoLogin {

    /**
     * session_id : 96c88085e8d4bbfbd4942d41daef570c
     * latitude : 40.98348973
     * longitude : 87.987324987
     * source : basilbox
     * locale : en
     * device_token : FHYCH4853fHcne3
     * platform_id : 1
     */

    private String session_id;
    private String latitude;
    private String longitude;
    private String source;
    private String locale;
    private String device_token;
    private int platform_id;

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public int getPlatform_id() {
        return platform_id;
    }

    public void setPlatform_id(int platform_id) {
        this.platform_id = platform_id;
    }
}
