package com.example.project2.model;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;



public class User
{


    /**
     * result : true
     * session_id : 96c88085e8d4bbfbd4942d41daef570c
     * serial : 83ch9h3dfjd0dl
     * qr_code : 4Np3mdlPyJtjTG0c
     * qr_code_pattern : $$PAYLOAD$$
     * loyalty_qr_code_pattern : $$PAYLOAD$$LY
     * offline_qr_code : 4Np3mdlPyJtjTG0c
     * places : [{"id":"1","name":"Starbucket","logo_url":"http://assets.smoothpay.com/store_images/12.png","website_url":"http://www.starbucket.for.you","distance":"120.00m","latitude":"43.65006140","longitude":"-79.37902010","address":"33 Victoria Street, Toronto, ON, CA, M5C 3A1, Just above Service Ontario","hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"ordering_hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"open":true,"accepting_orders":true,"category":"coffee","zuppler":"http://zuppler.com","is_fav":"false","is_in_passbook":"false","is_order_ahead_enabled":true,"is_delivery_enabled":true,"deals":[{"id":"1","name":"Spend $80 get $4","value":"$4"}],"progress_value":"$4","available":"$13","percent":"0","progress":"Progress $0.00/$80.00","is_new":"false"}]
     * account_info : {"result":"true","referral_message":"Download the SmoothPay app and use code %%CODE%% for $10 off your first order!","referral_code":"DH78JD","contact":{"first_name":"Lou","last_name":"Zar","email":"lou@example.com","sms_number":"4169671111","address":"44 Victoria Street, Toronsto, ON, CA, M5C 1Y2","apt":"1500","instruction":"Left of elevators","lat":"43.6532","lon":"-79.3832","full_name":"Lou Zar"},"cards":[{"card_id":"1","last_4":"4242","type":"gift_card","balance":8,"expiry":"2008"}],"credits":{"used":"0.00","smooth":"101.00","store":"42.00","total":"143.00","current_points":100,"lifetime_points":100},"last_transaction":{"date":"17-03-04","transaction_id":"3","store_name":"Pho King","purchase":"2.00","credit":"1.00","points_earned":"0","points_redeemed":"0","tip":"0","total_charge":"1.00","store_id":"1","logo_url":"http://assets.smoothpay.com/store_images/10.png","available":"$19","percent":"0","card_type":"gift_card","last_four":"4242","gift_card_balance":4.62},"transactions":[{"transaction_id":"3","date":"17-03-03","tip":"0.20","amount":"2.00","credit":"1.00","points_earned":"0","points_redeemed":"0","store":"Pho King"}],"credit_screen":{"earned_with_text_row1":"View Credit Details","earned_with_text_row2":"Since 2017","available":"143.00","first_row":"0","second_row":"101","third_row":"42","store_list":[{"id":"1","name":"Starbucket","logo_url":"http://assets.smoothpay.com/store_images/12.png","website_url":"http://www.starbucket.for.you","distance":"120.00m","latitude":"43.65006140","longitude":"-79.37902010","address":"33 Victoria Street, Toronto, ON, CA, M5C 3A1, Just above Service Ontario","hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"ordering_hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"open":true,"accepting_orders":true,"category":"coffee","zuppler":"http://zuppler.com","is_fav":"false","is_in_passbook":"false","is_order_ahead_enabled":true,"is_delivery_enabled":true,"deals":[{"id":"1","name":"Spend $80 get $4","value":"$4"}],"progress_value":"$4","available":"$13","percent":"0","progress":"Progress $0.00/$80.00","is_new":"false"}]}}
     * feed : [{"position":"1","card_type":"credit_nearby","cards":[{"position":"1","title":"My nearby credits","subText":"Starbucket","website":"http://www.starbucket.for.you","image_src":"http://assets.smoothpay.com/store_images/12.png","store_id":"3","credit":"13"}]}]
     */

    private String result;
    private String session_id;
    @PrimaryKey private String serial;
    private String qr_code;
    private String qr_code_pattern;
    private String loyalty_qr_code_pattern;
    private String offline_qr_code;
    private AccountInfo account_info;
    private List<Places> places;
    private List<Feed> feed;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    public String getQr_code_pattern() {
        return qr_code_pattern;
    }

    public void setQr_code_pattern(String qr_code_pattern) {
        this.qr_code_pattern = qr_code_pattern;
    }

    public String getLoyalty_qr_code_pattern() {
        return loyalty_qr_code_pattern;
    }

    public void setLoyalty_qr_code_pattern(String loyalty_qr_code_pattern) {
        this.loyalty_qr_code_pattern = loyalty_qr_code_pattern;
    }

    public String getOffline_qr_code() {
        return offline_qr_code;
    }

    public void setOffline_qr_code(String offline_qr_code) {
        this.offline_qr_code = offline_qr_code;
    }

    public AccountInfo getAccount_info() {
        return account_info;
    }

    public void setAccount_info(AccountInfo account_info) {
        this.account_info = account_info;
    }

    public List<Places> getPlaces() {
        return places;
    }

    public void setPlaces(List<Places> places) {
        this.places = places;
    }

    public List<Feed> getFeed() {
        return feed;
    }

    public void setFeed(List<Feed> feed) {
        this.feed = feed;
    }

    public static class AccountInfo {
        /**
         * result : true
         * referral_message : Download the SmoothPay app and use code %%CODE%% for $10 off your first order!
         * referral_code : DH78JD
         * contact : {"first_name":"Lou","last_name":"Zar","email":"lou@example.com","sms_number":"4169671111","address":"44 Victoria Street, Toronsto, ON, CA, M5C 1Y2","apt":"1500","instruction":"Left of elevators","lat":"43.6532","lon":"-79.3832","full_name":"Lou Zar"}
         * cards : [{"card_id":"1","last_4":"4242","type":"gift_card","balance":8,"expiry":"2008"}]
         * credits : {"used":"0.00","smooth":"101.00","store":"42.00","total":"143.00","current_points":100,"lifetime_points":100}
         * last_transaction : {"date":"17-03-04","transaction_id":"3","store_name":"Pho King","purchase":"2.00","credit":"1.00","points_earned":"0","points_redeemed":"0","tip":"0","total_charge":"1.00","store_id":"1","logo_url":"http://assets.smoothpay.com/store_images/10.png","available":"$19","percent":"0","card_type":"gift_card","last_four":"4242","gift_card_balance":4.62}
         * transactions : [{"transaction_id":"3","date":"17-03-03","tip":"0.20","amount":"2.00","credit":"1.00","points_earned":"0","points_redeemed":"0","store":"Pho King"}]
         * credit_screen : {"earned_with_text_row1":"View Credit Details","earned_with_text_row2":"Since 2017","available":"143.00","first_row":"0","second_row":"101","third_row":"42","store_list":[{"id":"1","name":"Starbucket","logo_url":"http://assets.smoothpay.com/store_images/12.png","website_url":"http://www.starbucket.for.you","distance":"120.00m","latitude":"43.65006140","longitude":"-79.37902010","address":"33 Victoria Street, Toronto, ON, CA, M5C 3A1, Just above Service Ontario","hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"ordering_hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"open":true,"accepting_orders":true,"category":"coffee","zuppler":"http://zuppler.com","is_fav":"false","is_in_passbook":"false","is_order_ahead_enabled":true,"is_delivery_enabled":true,"deals":[{"id":"1","name":"Spend $80 get $4","value":"$4"}],"progress_value":"$4","available":"$13","percent":"0","progress":"Progress $0.00/$80.00","is_new":"false"}]}
         */

        private String result;
        private String referral_message;
        private String referral_code;
        private Contact contact;
        private Credits credits;
        private LastTransaction last_transaction;
        private CreditScreen credit_screen;
        private List<Cards> cards;
        private List<Transactions> transactions;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getReferral_message() {
            return referral_message;
        }

        public void setReferral_message(String referral_message) {
            this.referral_message = referral_message;
        }

        public String getReferral_code() {
            return referral_code;
        }

        public void setReferral_code(String referral_code) {
            this.referral_code = referral_code;
        }

        public Contact getContact() {
            return contact;
        }

        public void setContact(Contact contact) {
            this.contact = contact;
        }

        public Credits getCredits() {
            return credits;
        }

        public void setCredits(Credits credits) {
            this.credits = credits;
        }

        public LastTransaction getLast_transaction() {
            return last_transaction;
        }

        public void setLast_transaction(LastTransaction last_transaction) {
            this.last_transaction = last_transaction;
        }

        public CreditScreen getCredit_screen() {
            return credit_screen;
        }

        public void setCredit_screen(CreditScreen credit_screen) {
            this.credit_screen = credit_screen;
        }

        public List<Cards> getCards() {
            return cards;
        }

        public void setCards(List<Cards> cards) {
            this.cards = cards;
        }

        public List<Transactions> getTransactions() {
            return transactions;
        }

        public void setTransactions(List<Transactions> transactions) {
            this.transactions = transactions;
        }

        public static class Contact {
            /**
             * first_name : Lou
             * last_name : Zar
             * email : lou@example.com
             * sms_number : 4169671111
             * address : 44 Victoria Street, Toronsto, ON, CA, M5C 1Y2
             * apt : 1500
             * instruction : Left of elevators
             * lat : 43.6532
             * lon : -79.3832
             * full_name : Lou Zar
             */

            private String first_name;
            private String last_name;
            private String email;
            private String sms_number;
            private String address;
            private String apt;
            private String instruction;
            private String lat;
            private String lon;
            private String full_name;

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getSms_number() {
                return sms_number;
            }

            public void setSms_number(String sms_number) {
                this.sms_number = sms_number;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getApt() {
                return apt;
            }

            public void setApt(String apt) {
                this.apt = apt;
            }

            public String getInstruction() {
                return instruction;
            }

            public void setInstruction(String instruction) {
                this.instruction = instruction;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getFull_name() {
                return full_name;
            }

            public void setFull_name(String full_name) {
                this.full_name = full_name;
            }
        }

        public static class Credits {
            /**
             * used : 0.00
             * smooth : 101.00
             * store : 42.00
             * total : 143.00
             * current_points : 100
             * lifetime_points : 100
             */

            private String used;
            private String smooth;
            private String store;
            private String total;
            private int current_points;
            private int lifetime_points;

            public String getUsed() {
                return used;
            }

            public void setUsed(String used) {
                this.used = used;
            }

            public String getSmooth() {
                return smooth;
            }

            public void setSmooth(String smooth) {
                this.smooth = smooth;
            }

            public String getStore() {
                return store;
            }

            public void setStore(String store) {
                this.store = store;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public int getCurrent_points() {
                return current_points;
            }

            public void setCurrent_points(int current_points) {
                this.current_points = current_points;
            }

            public int getLifetime_points() {
                return lifetime_points;
            }

            public void setLifetime_points(int lifetime_points) {
                this.lifetime_points = lifetime_points;
            }
        }

        public static class LastTransaction {
            /**
             * date : 17-03-04
             * transaction_id : 3
             * store_name : Pho King
             * purchase : 2.00
             * credit : 1.00
             * points_earned : 0
             * points_redeemed : 0
             * tip : 0
             * total_charge : 1.00
             * store_id : 1
             * logo_url : http://assets.smoothpay.com/store_images/10.png
             * available : $19
             * percent : 0
             * card_type : gift_card
             * last_four : 4242
             * gift_card_balance : 4.62
             */

            private String date;
            private String transaction_id;
            private String store_name;
            private String purchase;
            private String credit;
            private String points_earned;
            private String points_redeemed;
            private String tip;
            private String total_charge;
            private String store_id;
            private String logo_url;
            private String available;
            private String percent;
            private String card_type;
            private String last_four;
            private double gift_card_balance;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getTransaction_id() {
                return transaction_id;
            }

            public void setTransaction_id(String transaction_id) {
                this.transaction_id = transaction_id;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getPurchase() {
                return purchase;
            }

            public void setPurchase(String purchase) {
                this.purchase = purchase;
            }

            public String getCredit() {
                return credit;
            }

            public void setCredit(String credit) {
                this.credit = credit;
            }

            public String getPoints_earned() {
                return points_earned;
            }

            public void setPoints_earned(String points_earned) {
                this.points_earned = points_earned;
            }

            public String getPoints_redeemed() {
                return points_redeemed;
            }

            public void setPoints_redeemed(String points_redeemed) {
                this.points_redeemed = points_redeemed;
            }

            public String getTip() {
                return tip;
            }

            public void setTip(String tip) {
                this.tip = tip;
            }

            public String getTotal_charge() {
                return total_charge;
            }

            public void setTotal_charge(String total_charge) {
                this.total_charge = total_charge;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getLogo_url() {
                return logo_url;
            }

            public void setLogo_url(String logo_url) {
                this.logo_url = logo_url;
            }

            public String getAvailable() {
                return available;
            }

            public void setAvailable(String available) {
                this.available = available;
            }

            public String getPercent() {
                return percent;
            }

            public void setPercent(String percent) {
                this.percent = percent;
            }

            public String getCard_type() {
                return card_type;
            }

            public void setCard_type(String card_type) {
                this.card_type = card_type;
            }

            public String getLast_four() {
                return last_four;
            }

            public void setLast_four(String last_four) {
                this.last_four = last_four;
            }

            public double getGift_card_balance() {
                return gift_card_balance;
            }

            public void setGift_card_balance(double gift_card_balance) {
                this.gift_card_balance = gift_card_balance;
            }
        }

        public static class CreditScreen {
            /**
             * earned_with_text_row1 : View Credit Details
             * earned_with_text_row2 : Since 2017
             * available : 143.00
             * first_row : 0
             * second_row : 101
             * third_row : 42
             * store_list : [{"id":"1","name":"Starbucket","logo_url":"http://assets.smoothpay.com/store_images/12.png","website_url":"http://www.starbucket.for.you","distance":"120.00m","latitude":"43.65006140","longitude":"-79.37902010","address":"33 Victoria Street, Toronto, ON, CA, M5C 3A1, Just above Service Ontario","hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"ordering_hours":{"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"},"open":true,"accepting_orders":true,"category":"coffee","zuppler":"http://zuppler.com","is_fav":"false","is_in_passbook":"false","is_order_ahead_enabled":true,"is_delivery_enabled":true,"deals":[{"id":"1","name":"Spend $80 get $4","value":"$4"}],"progress_value":"$4","available":"$13","percent":"0","progress":"Progress $0.00/$80.00","is_new":"false"}]
             */

            private String earned_with_text_row1;
            private String earned_with_text_row2;
            private String available;
            private String first_row;
            private String second_row;
            private String third_row;
            private List<StoreList> store_list;

            public String getEarned_with_text_row1() {
                return earned_with_text_row1;
            }

            public void setEarned_with_text_row1(String earned_with_text_row1) {
                this.earned_with_text_row1 = earned_with_text_row1;
            }

            public String getEarned_with_text_row2() {
                return earned_with_text_row2;
            }

            public void setEarned_with_text_row2(String earned_with_text_row2) {
                this.earned_with_text_row2 = earned_with_text_row2;
            }

            public String getAvailable() {
                return available;
            }

            public void setAvailable(String available) {
                this.available = available;
            }

            public String getFirst_row() {
                return first_row;
            }

            public void setFirst_row(String first_row) {
                this.first_row = first_row;
            }

            public String getSecond_row() {
                return second_row;
            }

            public void setSecond_row(String second_row) {
                this.second_row = second_row;
            }

            public String getThird_row() {
                return third_row;
            }

            public void setThird_row(String third_row) {
                this.third_row = third_row;
            }

            public List<StoreList> getStore_list() {
                return store_list;
            }

            public void setStore_list(List<StoreList> store_list) {
                this.store_list = store_list;
            }

            public static class StoreList {
                /**
                 * id : 1
                 * name : Starbucket
                 * logo_url : http://assets.smoothpay.com/store_images/12.png
                 * website_url : http://www.starbucket.for.you
                 * distance : 120.00m
                 * latitude : 43.65006140
                 * longitude : -79.37902010
                 * address : 33 Victoria Street, Toronto, ON, CA, M5C 3A1, Just above Service Ontario
                 * hours : {"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"}
                 * ordering_hours : {"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"}
                 * open : true
                 * accepting_orders : true
                 * category : coffee
                 * zuppler : http://zuppler.com
                 * is_fav : false
                 * is_in_passbook : false
                 * is_order_ahead_enabled : true
                 * is_delivery_enabled : true
                 * deals : [{"id":"1","name":"Spend $80 get $4","value":"$4"}]
                 * progress_value : $4
                 * available : $13
                 * percent : 0
                 * progress : Progress $0.00/$80.00
                 * is_new : false
                 */

                private String id;
                private String name;
                private String logo_url;
                private String website_url;
                private String distance;
                private String latitude;
                private String longitude;
                private String address;
                private Hours hours;
                private OrderingHours ordering_hours;
                private boolean open;
                private boolean accepting_orders;
                private String category;
                private String zuppler;
                private String is_fav;
                private String is_in_passbook;
                private boolean is_order_ahead_enabled;
                private boolean is_delivery_enabled;
                private String progress_value;
                private String available;
                private String percent;
                private String progress;
                private String is_new;
                private List<Deals> deals;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getLogo_url() {
                    return logo_url;
                }

                public void setLogo_url(String logo_url) {
                    this.logo_url = logo_url;
                }

                public String getWebsite_url() {
                    return website_url;
                }

                public void setWebsite_url(String website_url) {
                    this.website_url = website_url;
                }

                public String getDistance() {
                    return distance;
                }

                public void setDistance(String distance) {
                    this.distance = distance;
                }

                public String getLatitude() {
                    return latitude;
                }

                public void setLatitude(String latitude) {
                    this.latitude = latitude;
                }

                public String getLongitude() {
                    return longitude;
                }

                public void setLongitude(String longitude) {
                    this.longitude = longitude;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public Hours getHours() {
                    return hours;
                }

                public void setHours(Hours hours) {
                    this.hours = hours;
                }

                public OrderingHours getOrdering_hours() {
                    return ordering_hours;
                }

                public void setOrdering_hours(OrderingHours ordering_hours) {
                    this.ordering_hours = ordering_hours;
                }

                public boolean isOpen() {
                    return open;
                }

                public void setOpen(boolean open) {
                    this.open = open;
                }

                public boolean isAccepting_orders() {
                    return accepting_orders;
                }

                public void setAccepting_orders(boolean accepting_orders) {
                    this.accepting_orders = accepting_orders;
                }

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public String getZuppler() {
                    return zuppler;
                }

                public void setZuppler(String zuppler) {
                    this.zuppler = zuppler;
                }

                public String getIs_fav() {
                    return is_fav;
                }

                public void setIs_fav(String is_fav) {
                    this.is_fav = is_fav;
                }

                public String getIs_in_passbook() {
                    return is_in_passbook;
                }

                public void setIs_in_passbook(String is_in_passbook) {
                    this.is_in_passbook = is_in_passbook;
                }

                public boolean isIs_order_ahead_enabled() {
                    return is_order_ahead_enabled;
                }

                public void setIs_order_ahead_enabled(boolean is_order_ahead_enabled) {
                    this.is_order_ahead_enabled = is_order_ahead_enabled;
                }

                public boolean isIs_delivery_enabled() {
                    return is_delivery_enabled;
                }

                public void setIs_delivery_enabled(boolean is_delivery_enabled) {
                    this.is_delivery_enabled = is_delivery_enabled;
                }

                public String getProgress_value() {
                    return progress_value;
                }

                public void setProgress_value(String progress_value) {
                    this.progress_value = progress_value;
                }

                public String getAvailable() {
                    return available;
                }

                public void setAvailable(String available) {
                    this.available = available;
                }

                public String getPercent() {
                    return percent;
                }

                public void setPercent(String percent) {
                    this.percent = percent;
                }

                public String getProgress() {
                    return progress;
                }

                public void setProgress(String progress) {
                    this.progress = progress;
                }

                public String getIs_new() {
                    return is_new;
                }

                public void setIs_new(String is_new) {
                    this.is_new = is_new;
                }

                public List<Deals> getDeals() {
                    return deals;
                }

                public void setDeals(List<Deals> deals) {
                    this.deals = deals;
                }

                public static class Hours {
                    /**
                     * monday : 02:00 AM - 08:00 PM
                     * tuesday : 02:00 AM - 08:00 PM
                     * wednesday : 02:00 AM - 08:00 PM
                     * thursday : 02:00 AM - 08:00 PM
                     * friday : 02:00 AM - 08:00 PM
                     * saturday : 02:00 AM - 08:00 PM
                     * sunday : Closed
                     */

                    private String monday;
                    private String tuesday;
                    private String wednesday;
                    private String thursday;
                    private String friday;
                    private String saturday;
                    private String sunday;

                    public String getMonday() {
                        return monday;
                    }

                    public void setMonday(String monday) {
                        this.monday = monday;
                    }

                    public String getTuesday() {
                        return tuesday;
                    }

                    public void setTuesday(String tuesday) {
                        this.tuesday = tuesday;
                    }

                    public String getWednesday() {
                        return wednesday;
                    }

                    public void setWednesday(String wednesday) {
                        this.wednesday = wednesday;
                    }

                    public String getThursday() {
                        return thursday;
                    }

                    public void setThursday(String thursday) {
                        this.thursday = thursday;
                    }

                    public String getFriday() {
                        return friday;
                    }

                    public void setFriday(String friday) {
                        this.friday = friday;
                    }

                    public String getSaturday() {
                        return saturday;
                    }

                    public void setSaturday(String saturday) {
                        this.saturday = saturday;
                    }

                    public String getSunday() {
                        return sunday;
                    }

                    public void setSunday(String sunday) {
                        this.sunday = sunday;
                    }
                }

                public static class OrderingHours {
                    /**
                     * monday : 02:00 AM - 08:00 PM
                     * tuesday : 02:00 AM - 08:00 PM
                     * wednesday : 02:00 AM - 08:00 PM
                     * thursday : 02:00 AM - 08:00 PM
                     * friday : 02:00 AM - 08:00 PM
                     * saturday : 02:00 AM - 08:00 PM
                     * sunday : Closed
                     */

                    private String monday;
                    private String tuesday;
                    private String wednesday;
                    private String thursday;
                    private String friday;
                    private String saturday;
                    private String sunday;

                    public String getMonday() {
                        return monday;
                    }

                    public void setMonday(String monday) {
                        this.monday = monday;
                    }

                    public String getTuesday() {
                        return tuesday;
                    }

                    public void setTuesday(String tuesday) {
                        this.tuesday = tuesday;
                    }

                    public String getWednesday() {
                        return wednesday;
                    }

                    public void setWednesday(String wednesday) {
                        this.wednesday = wednesday;
                    }

                    public String getThursday() {
                        return thursday;
                    }

                    public void setThursday(String thursday) {
                        this.thursday = thursday;
                    }

                    public String getFriday() {
                        return friday;
                    }

                    public void setFriday(String friday) {
                        this.friday = friday;
                    }

                    public String getSaturday() {
                        return saturday;
                    }

                    public void setSaturday(String saturday) {
                        this.saturday = saturday;
                    }

                    public String getSunday() {
                        return sunday;
                    }

                    public void setSunday(String sunday) {
                        this.sunday = sunday;
                    }
                }

                public static class Deals {
                    /**
                     * id : 1
                     * name : Spend $80 get $4
                     * value : $4
                     */

                    private String id;
                    private String name;
                    private String value;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }
                }
            }
        }

        public static class Cards {
            /**
             * card_id : 1
             * last_4 : 4242
             * type : gift_card
             * balance : 8
             * expiry : 2008
             */

            private String card_id;
            private String last_4;
            private String type;
            private int balance;
            private String expiry;

            public String getCard_id() {
                return card_id;
            }

            public void setCard_id(String card_id) {
                this.card_id = card_id;
            }

            public String getLast_4() {
                return last_4;
            }

            public void setLast_4(String last_4) {
                this.last_4 = last_4;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public int getBalance() {
                return balance;
            }

            public void setBalance(int balance) {
                this.balance = balance;
            }

            public String getExpiry() {
                return expiry;
            }

            public void setExpiry(String expiry) {
                this.expiry = expiry;
            }
        }

        public static class Transactions {
            /**
             * transaction_id : 3
             * date : 17-03-03
             * tip : 0.20
             * amount : 2.00
             * credit : 1.00
             * points_earned : 0
             * points_redeemed : 0
             * store : Pho King
             */

            private String transaction_id;
            private String date;
            private String tip;
            private String amount;
            private String credit;
            private String points_earned;
            private String points_redeemed;
            private String store;

            public String getTransaction_id() {
                return transaction_id;
            }

            public void setTransaction_id(String transaction_id) {
                this.transaction_id = transaction_id;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getTip() {
                return tip;
            }

            public void setTip(String tip) {
                this.tip = tip;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getCredit() {
                return credit;
            }

            public void setCredit(String credit) {
                this.credit = credit;
            }

            public String getPoints_earned() {
                return points_earned;
            }

            public void setPoints_earned(String points_earned) {
                this.points_earned = points_earned;
            }

            public String getPoints_redeemed() {
                return points_redeemed;
            }

            public void setPoints_redeemed(String points_redeemed) {
                this.points_redeemed = points_redeemed;
            }

            public String getStore() {
                return store;
            }

            public void setStore(String store) {
                this.store = store;
            }
        }
    }

    public static class Places {
        /**
         * id : 1
         * name : Starbucket
         * logo_url : http://assets.smoothpay.com/store_images/12.png
         * website_url : http://www.starbucket.for.you
         * distance : 120.00m
         * latitude : 43.65006140
         * longitude : -79.37902010
         * address : 33 Victoria Street, Toronto, ON, CA, M5C 3A1, Just above Service Ontario
         * hours : {"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"}
         * ordering_hours : {"monday":"02:00 AM - 08:00 PM","tuesday":"02:00 AM - 08:00 PM","wednesday":"02:00 AM - 08:00 PM","thursday":"02:00 AM - 08:00 PM","friday":"02:00 AM - 08:00 PM","saturday":"02:00 AM - 08:00 PM","sunday":"Closed"}
         * open : true
         * accepting_orders : true
         * category : coffee
         * zuppler : http://zuppler.com
         * is_fav : false
         * is_in_passbook : false
         * is_order_ahead_enabled : true
         * is_delivery_enabled : true
         * deals : [{"id":"1","name":"Spend $80 get $4","value":"$4"}]
         * progress_value : $4
         * available : $13
         * percent : 0
         * progress : Progress $0.00/$80.00
         * is_new : false
         */

        private String id;
        private String name;
        private String logo_url;
        private String website_url;
        private String distance;
        private String latitude;
        private String longitude;
        private String address;
        private HoursX hours;
        private OrderingHoursX ordering_hours;
        private boolean open;
        private boolean accepting_orders;
        private String category;
        private String zuppler;
        private String is_fav;
        private String is_in_passbook;
        private boolean is_order_ahead_enabled;
        private boolean is_delivery_enabled;
        private String progress_value;
        private String available;
        private String percent;
        private String progress;
        private String is_new;
        private List<DealsX> deals;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo_url() {
            return logo_url;
        }

        public void setLogo_url(String logo_url) {
            this.logo_url = logo_url;
        }

        public String getWebsite_url() {
            return website_url;
        }

        public void setWebsite_url(String website_url) {
            this.website_url = website_url;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public HoursX getHours() {
            return hours;
        }

        public void setHours(HoursX hours) {
            this.hours = hours;
        }

        public OrderingHoursX getOrdering_hours() {
            return ordering_hours;
        }

        public void setOrdering_hours(OrderingHoursX ordering_hours) {
            this.ordering_hours = ordering_hours;
        }

        public boolean isOpen() {
            return open;
        }

        public void setOpen(boolean open) {
            this.open = open;
        }

        public boolean isAccepting_orders() {
            return accepting_orders;
        }

        public void setAccepting_orders(boolean accepting_orders) {
            this.accepting_orders = accepting_orders;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getZuppler() {
            return zuppler;
        }

        public void setZuppler(String zuppler) {
            this.zuppler = zuppler;
        }

        public String getIs_fav() {
            return is_fav;
        }

        public void setIs_fav(String is_fav) {
            this.is_fav = is_fav;
        }

        public String getIs_in_passbook() {
            return is_in_passbook;
        }

        public void setIs_in_passbook(String is_in_passbook) {
            this.is_in_passbook = is_in_passbook;
        }

        public boolean isIs_order_ahead_enabled() {
            return is_order_ahead_enabled;
        }

        public void setIs_order_ahead_enabled(boolean is_order_ahead_enabled) {
            this.is_order_ahead_enabled = is_order_ahead_enabled;
        }

        public boolean isIs_delivery_enabled() {
            return is_delivery_enabled;
        }

        public void setIs_delivery_enabled(boolean is_delivery_enabled) {
            this.is_delivery_enabled = is_delivery_enabled;
        }

        public String getProgress_value() {
            return progress_value;
        }

        public void setProgress_value(String progress_value) {
            this.progress_value = progress_value;
        }

        public String getAvailable() {
            return available;
        }

        public void setAvailable(String available) {
            this.available = available;
        }

        public String getPercent() {
            return percent;
        }

        public void setPercent(String percent) {
            this.percent = percent;
        }

        public String getProgress() {
            return progress;
        }

        public void setProgress(String progress) {
            this.progress = progress;
        }

        public String getIs_new() {
            return is_new;
        }

        public void setIs_new(String is_new) {
            this.is_new = is_new;
        }

        public List<DealsX> getDeals() {
            return deals;
        }

        public void setDeals(List<DealsX> deals) {
            this.deals = deals;
        }

        public static class HoursX {
            /**
             * monday : 02:00 AM - 08:00 PM
             * tuesday : 02:00 AM - 08:00 PM
             * wednesday : 02:00 AM - 08:00 PM
             * thursday : 02:00 AM - 08:00 PM
             * friday : 02:00 AM - 08:00 PM
             * saturday : 02:00 AM - 08:00 PM
             * sunday : Closed
             */

            private String monday;
            private String tuesday;
            private String wednesday;
            private String thursday;
            private String friday;
            private String saturday;
            private String sunday;

            public String getMonday() {
                return monday;
            }

            public void setMonday(String monday) {
                this.monday = monday;
            }

            public String getTuesday() {
                return tuesday;
            }

            public void setTuesday(String tuesday) {
                this.tuesday = tuesday;
            }

            public String getWednesday() {
                return wednesday;
            }

            public void setWednesday(String wednesday) {
                this.wednesday = wednesday;
            }

            public String getThursday() {
                return thursday;
            }

            public void setThursday(String thursday) {
                this.thursday = thursday;
            }

            public String getFriday() {
                return friday;
            }

            public void setFriday(String friday) {
                this.friday = friday;
            }

            public String getSaturday() {
                return saturday;
            }

            public void setSaturday(String saturday) {
                this.saturday = saturday;
            }

            public String getSunday() {
                return sunday;
            }

            public void setSunday(String sunday) {
                this.sunday = sunday;
            }
        }

        public static class OrderingHoursX {
            /**
             * monday : 02:00 AM - 08:00 PM
             * tuesday : 02:00 AM - 08:00 PM
             * wednesday : 02:00 AM - 08:00 PM
             * thursday : 02:00 AM - 08:00 PM
             * friday : 02:00 AM - 08:00 PM
             * saturday : 02:00 AM - 08:00 PM
             * sunday : Closed
             */

            private String monday;
            private String tuesday;
            private String wednesday;
            private String thursday;
            private String friday;
            private String saturday;
            private String sunday;

            public String getMonday() {
                return monday;
            }

            public void setMonday(String monday) {
                this.monday = monday;
            }

            public String getTuesday() {
                return tuesday;
            }

            public void setTuesday(String tuesday) {
                this.tuesday = tuesday;
            }

            public String getWednesday() {
                return wednesday;
            }

            public void setWednesday(String wednesday) {
                this.wednesday = wednesday;
            }

            public String getThursday() {
                return thursday;
            }

            public void setThursday(String thursday) {
                this.thursday = thursday;
            }

            public String getFriday() {
                return friday;
            }

            public void setFriday(String friday) {
                this.friday = friday;
            }

            public String getSaturday() {
                return saturday;
            }

            public void setSaturday(String saturday) {
                this.saturday = saturday;
            }

            public String getSunday() {
                return sunday;
            }

            public void setSunday(String sunday) {
                this.sunday = sunday;
            }
        }

        public static class DealsX {
            /**
             * id : 1
             * name : Spend $80 get $4
             * value : $4
             */

            private String id;
            private String name;
            private String value;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }

    public static class Feed {
        /**
         * position : 1
         * card_type : credit_nearby
         * cards : [{"position":"1","title":"My nearby credits","subText":"Starbucket","website":"http://www.starbucket.for.you","image_src":"http://assets.smoothpay.com/store_images/12.png","store_id":"3","credit":"13"}]
         */

        private String position;
        private String card_type;
        private List<CardsX> cards;

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getCard_type() {
            return card_type;
        }

        public void setCard_type(String card_type) {
            this.card_type = card_type;
        }

        public List<CardsX> getCards() {
            return cards;
        }

        public void setCards(List<CardsX> cards) {
            this.cards = cards;
        }

        public static class CardsX {
            /**
             * position : 1
             * title : My nearby credits
             * subText : Starbucket
             * website : http://www.starbucket.for.you
             * image_src : http://assets.smoothpay.com/store_images/12.png
             * store_id : 3
             * credit : 13
             */

            private String position;
            private String title;
            private String subText;
            private String website;
            private String image_src;
            private String store_id;
            private String credit;

            public String getPosition() {
                return position;
            }

            public void setPosition(String position) {
                this.position = position;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubText() {
                return subText;
            }

            public void setSubText(String subText) {
                this.subText = subText;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }

            public String getImage_src() {
                return image_src;
            }

            public void setImage_src(String image_src) {
                this.image_src = image_src;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getCredit() {
                return credit;
            }

            public void setCredit(String credit) {
                this.credit = credit;
            }
        }
    }
}
