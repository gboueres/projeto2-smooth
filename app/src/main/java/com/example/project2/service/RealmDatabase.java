package com.example.project2.service;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmDatabase {

    public static Realm realm()
    {
        RealmConfiguration realmConfig = new RealmConfiguration
                .Builder().build();
        Realm realm = Realm.getInstance(realmConfig);
        System.out.println(realmConfig.getPath());
        return realm;
    }


}