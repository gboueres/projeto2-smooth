package com.example.project2.model.gets;

import java.util.List;

public class Business {
    private List<DataBusiness> data;

    public List<DataBusiness> getData() {

        return data;
    }

    public void setData(List<DataBusiness> data) {

        this.data = data;
    }
}
