package com.example.project2.model;

public class Item {


    /**
     * feed_item_id : 1703
     * title : Try our Mango Ginger Ale Soda
     * subtitle : Quench your thirst
     * description : null
     * background_image_url : http://assets.smoothpay.com/store_images/basilbox/menu/MangoGingerAleSoda.jpg
     * thumbnail_image_url : null
     * button_label : null
     * button_action : null
     * card_action : basilbox://products/2
     */

    private int feed_item_id;
    private String title;
    private String subtitle;
    private String description;
    private String background_image_url;
    private String thumbnail_image_url;
    private String button_label;
    private String button_action;
    private String card_action;

    public int getFeed_item_id() {

        return feed_item_id;
    }

    public void setFeed_item_id(int feed_item_id) {

        this.feed_item_id = feed_item_id;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getSubtitle() {

        return subtitle;
    }

    public void setSubtitle(String subtitle) {

        this.subtitle = subtitle;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getBackground_image_url() {

        return background_image_url;
    }

    public void setBackground_image_url(String background_image_url) {

        this.background_image_url = background_image_url;
    }

    public Object getThumbnail_image_url() {

        return thumbnail_image_url;
    }

    public void setThumbnail_image_url(String thumbnail_image_url) {

        this.thumbnail_image_url = thumbnail_image_url;
    }

    public Object getButton_label() {

        return button_label;
    }

    public void setButton_label(String button_label) {

        this.button_label = button_label;
    }

    public Object getButton_action() {

        return button_action;
    }

    public void setButton_action(String button_action) {

        this.button_action = button_action;
    }

    public String getCard_action() {

        return card_action;
    }

    public void setCard_action(String card_action) {

        this.card_action = card_action;
    }
}
