package com.example.project2.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SmoothService {

    private Retrofit retrofit = null;

    public SmoothApi getSmoothApi() {
        String BASE_URL = "http://api.dev.smoothpay.com/app_dev.php/";

        if (retrofit == null) {
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit.create(SmoothApi.class);
    }
}
