package com.example.project2.view.login;

import com.example.project2.model.User;
import com.example.project2.model.UserAutoLogin;
import com.example.project2.view.BaseView;

public interface LoginView extends BaseView {

    void confirmLogin(User user);
    void confirmAutoLogin(UserAutoLogin userautologin);

    void showLoginIncorrect(String result);
}
