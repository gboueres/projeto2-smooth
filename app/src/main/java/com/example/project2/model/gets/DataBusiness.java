package com.example.project2.model.gets;

import java.util.List;

public class DataBusiness {

    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        /**
         * id : 250
         * name : Royal Bank Plaza
         * image_url :
         * website_url : http://www.thebasilbox.com
         * distance : 9155.36km
         * currency : CAD
         * phone : 4163648000
         * timezone : America/Toronto
         * latitude : 43.6464188
         * longitude : -79.3800207
         * line1 : 200 Bay St.
         * line2 :
         * line3 :
         * city : Toronto
         * state : ON
         * zip : M5J 2J2
         * country : CA
         * details :
         * open : true
         * accepting_orders : true
         * is_delivery_enabled : false
         * is_order_ahead_enabled : true
         * hours_overridden : false
         * tags : []
         * hours : {"monday":["07:00 AM - 06:30 PM"],"tuesday":["07:00 AM - 06:30 PM"],"wednesday":["07:00 AM - 06:30 PM"],"thursday":["07:00 AM - 06:30 PM"],"friday":["07:00 AM - 06:30 PM"],"saturday":[],"sunday":[]}
         * ordering_hours : {"monday":["07:00 AM - 06:30 PM"],"tuesday":["07:00 AM - 06:30 PM"],"wednesday":["07:00 AM - 06:30 PM"],"thursday":["07:00 AM - 06:30 PM"],"friday":["07:00 AM - 06:30 PM"],"saturday":[],"sunday":[]}
         */

        private int id;
        private String name;
        private String image_url;
        private String website_url;
        private String distance;
        private String currency;
        private String phone;
        private String timezone;
        private double latitude;
        private double longitude;
        private String line1;
        private String line2;
        private String line3;
        private String city;
        private String state;
        private String zip;
        private String country;
        private String details;
        private boolean open;
        private boolean accepting_orders;
        private boolean is_delivery_enabled;
        private boolean is_order_ahead_enabled;
        private boolean hours_overridden;
        private Hours hours;
        private OrderingHours ordering_hours;
        private List<String> tags;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getWebsite_url() {
            return website_url;
        }

        public void setWebsite_url(String website_url) {
            this.website_url = website_url;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getLine1() {
            return line1;
        }

        public void setLine1(String line1) {
            this.line1 = line1;
        }

        public String getLine2() {
            return line2;
        }

        public void setLine2(String line2) {
            this.line2 = line2;
        }

        public String getLine3() {
            return line3;
        }

        public void setLine3(String line3) {
            this.line3 = line3;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public boolean isOpen() {
            return open;
        }

        public void setOpen(boolean open) {
            this.open = open;
        }

        public boolean isAccepting_orders() {
            return accepting_orders;
        }

        public void setAccepting_orders(boolean accepting_orders) {
            this.accepting_orders = accepting_orders;
        }

        public boolean isIs_delivery_enabled() {
            return is_delivery_enabled;
        }

        public void setIs_delivery_enabled(boolean is_delivery_enabled) {
            this.is_delivery_enabled = is_delivery_enabled;
        }

        public boolean isIs_order_ahead_enabled() {
            return is_order_ahead_enabled;
        }

        public void setIs_order_ahead_enabled(boolean is_order_ahead_enabled) {
            this.is_order_ahead_enabled = is_order_ahead_enabled;
        }

        public boolean isHours_overridden() {
            return hours_overridden;
        }

        public void setHours_overridden(boolean hours_overridden) {
            this.hours_overridden = hours_overridden;
        }

        public Hours getHours() {
            return hours;
        }

        public void setHours(Hours hours) {
            this.hours = hours;
        }

        public OrderingHours getOrdering_hours() {
            return ordering_hours;
        }

        public void setOrdering_hours(OrderingHours ordering_hours) {
            this.ordering_hours = ordering_hours;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public static class Hours {
            private List<String> monday;
            private List<String> tuesday;
            private List<String> wednesday;
            private List<String> thursday;
            private List<String> friday;
            private List<String> saturday;
            private List<String> sunday;

            public List<String> getMonday() {
                return monday;
            }

            public void setMonday(List<String> monday) {
                this.monday = monday;
            }

            public List<String> getTuesday() {
                return tuesday;
            }

            public void setTuesday(List<String> tuesday) {
                this.tuesday = tuesday;
            }

            public List<String> getWednesday() {
                return wednesday;
            }

            public void setWednesday(List<String> wednesday) {
                this.wednesday = wednesday;
            }

            public List<String> getThursday() {
                return thursday;
            }

            public void setThursday(List<String> thursday) {
                this.thursday = thursday;
            }

            public List<String> getFriday() {
                return friday;
            }

            public void setFriday(List<String> friday) {
                this.friday = friday;
            }

            public List<?> getSaturday() {
                return saturday;
            }

            public void setSaturday(List<String> saturday) {
                this.saturday = saturday;
            }

            public List<String> getSunday() {
                return sunday;
            }

            public void setSunday(List<String> sunday) {
                this.sunday = sunday;
            }
        }

        public static class OrderingHours {
            private List<String> monday;
            private List<String> tuesday;
            private List<String> wednesday;
            private List<String> thursday;
            private List<String> friday;
            private List<String> saturday;
            private List<String> sunday;

            public List<String> getMonday() {
                return monday;
            }

            public void setMonday(List<String> monday) {
                this.monday = monday;
            }

            public List<String> getTuesday() {
                return tuesday;
            }

            public void setTuesday(List<String> tuesday) {
                this.tuesday = tuesday;
            }

            public List<String> getWednesday() {
                return wednesday;
            }

            public void setWednesday(List<String> wednesday) {
                this.wednesday = wednesday;
            }

            public List<String> getThursday() {
                return thursday;
            }

            public void setThursday(List<String> thursday) {
                this.thursday = thursday;
            }

            public List<String> getFriday() {
                return friday;
            }

            public void setFriday(List<String> friday) {
                this.friday = friday;
            }

            public List<?> getSaturday() {
                return saturday;
            }

            public void setSaturday(List<String> saturday) {
                this.saturday = saturday;
            }

            public List<?> getSunday() {
                return sunday;
            }

            public void setSunday(List<String> sunday) {
                this.sunday = sunday;
            }
        }
    }
}
