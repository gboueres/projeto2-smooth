package com.example.project2.model.gets;

import com.example.project2.model.Item;

import java.util.List;



public  class DataFeed {
    /**
     * feed_id : 3872
     * title : Offers
     * feed_type : all_offers
     * feed_style_type : horizontal_cards
     * list_action : basilbox://app/offers
     * list_action_label : basilbox://app/offers
     * tags : []
     * items : []
     */

    private int feed_id;
    private String title;
    private String feed_type;
    private String feed_style_type;
    private String list_action;
    private String list_action_label;
    private List<String> tags;
    private List<Item> items;

    public int getFeed_id() {

        return feed_id;
    }

    public void setFeed_id(int feed_id) {

        this.feed_id = feed_id;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getFeed_type() {

        return feed_type;
    }

    public void setFeed_type(String feed_type) {

        this.feed_type = feed_type;
    }

    public String getFeed_style_type() {

        return feed_style_type;
    }

    public void setFeed_style_type(String feed_style_type) {

        this.feed_style_type = feed_style_type;
    }

    public String getList_action() {

        return list_action;
    }

    public void setList_action(String list_action) {

        this.list_action = list_action;
    }

    public String getList_action_label() {

        return list_action_label;
    }

    public void setList_action_label(String list_action_label) {

        this.list_action_label = list_action_label;
    }

    public List<String> getTags() {

        return tags;
    }

    public void setTags(List<String> tags) {

        this.tags = tags;
    }

    public List<Item> getItems() {

        return items;
    }

    public void setItems(List<Item> items) {

        this.items = items;
    }


}
